# -*- coding: utf-8 -*-
"""
Created on Thu Aug 20 18:27:34 2020

@author: chrta
"""



### adapted from https://www.geeksforgeeks.org/convert-csv-to-json-using-python/
import csv 
import json 
import glob as glob
  
  
# Function to convert a CSV to JSON 
# Takes the file paths as arguments 
def make_json(csvFilePath, jsonFilePath): 
      
    # create a dictionary 
    data = {} 
      
    # Open a csv reader called DictReader 
    with open(csvFilePath, encoding='utf-8') as csvf: 
        csvReader = csv.DictReader(csvf) 
          
        # Convert each row into a dictionary  
        # and add it to data 
        for rows in csvReader: 
              
            # Assuming a column named 'No' to 
            # be the primary key 
            #print(rows)
            key = rows['name'] 
            data[key] = rows 
            # print(data[key])
            # print('\n')
  
    # Open a json writer, and use the json.dumps()  
    # function to dump data 
    with open(jsonFilePath, 'w', encoding='utf-8') as jsonf: 
        jsonf.write(json.dumps(data, indent=4)) 
    return data

# Decide the two file paths according to your  
# computer system 
    

path = r'F:\Nextcloud\zgaa\TRNSFRM2020\statistik'
found_files = glob.glob(path + '\*.csv')

for name in found_files:
    csvFilePath = name
    print(name[:-4])
    jsonFilePath = name[:-4] + '.json'
    # Call the make_json function 
    data = make_json(csvFilePath, jsonFilePath)
