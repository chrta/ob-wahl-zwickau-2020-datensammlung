# -*- coding: utf-8 -*-
"""
Created on Thu Aug 20 18:39:21 2020

@author: chrta
"""

### adapted from https://www.kite.com/python/answers/how-to-convert-a-%60.csv%60-file-to-%60json%60-in-python

import csv 
import json 

file = open("base_info_2.csv", "r")

dict_reader = csv.DictReader(file)


dict_from_csv = list(dict_reader)

json_from_csv = json.dumps(dict_from_csv,indent=4)


print(json_from_csv)

jsonFilePath = r'base_info2.json'
with open(jsonFilePath, 'w', encoding='utf-8') as jsonf: 
        jsonf.write(json_from_csv) 