# OB Wahl Zwickau 2020
Das ist eine Übersichtsseite zur Sammlung von Daten rund um die Wahl des Oberbürgermeisters im sächsischen Zwickau 2020. Alle Daten wurden aus öffentlichen Quellen gesammelt und werden hier ohne Wertung vorgehalten. Die Datensammlung erhebt keinen Anspruch auf Vollständigkeit. Außerdem werden Python-Scripts zur Verfügung gestellt, die ausgewählte Daten darstellen und zusammenfassen. Diese Darstellung ist unter Umständen nur eine Möglichkeit dies zu tun, wir möchten sie dazu ermutigen die Daten selber zu sichten, zu ergänzen oder auszuwerten. Alle Daten werden hier unter CC-SA-BY verteilt. 
Dies ist ein gemeinschaftsprojekt von [z-Labor e.V.](http://www.z-labor.space) und [#ZwickauGehtAuchAnders](https://zwickaugehtauchanders.tumblr.com/).  

### Rahmendaten

*Datum:* Ersttermin **20.09.2020**, Zweittermin **11.10.2020**  

*Kandidaten:*  

+ [Kathrin Köhler (CDU)](https://www.cduzwickau.de/kathrin-koehler) 
	
+ [Ute Brückner (Die Linke)](https://www.ute-brückner.de/)
	
+ [Constance Arndt (Bürger für Zwickau)](https://www.arndt-2020.de/)
	
+ [Andreas Gerold](https://www.ob-gerold-2020.de)
	
+ [Michael Jakob](https://www.michael-jakob-zwickau-oberburgermeisterkandidat.de/)

### Datenstruktur
` |-> .`  
` |-> wahlprogramme`  
` |-> scripts`  
	` |.. -> export`  
` |-> rohdaten`  
	` |.. -> koehler`  
	` |.. -> arndt`  
	` |.. -> brueckner`  
	` |.. -> gerold`  
	` |.. -> jakob`  

### Kontakt

Wir würden uns freuen, wenn sich andere an diesem Projekt beteiligen. Kontaktieren sie uns über [Mail](mailto:info@z-labor.space) oder kommen sie zu einem offenen Abend im z-Labor, Seilerstraße 1, 08056 Zwickau immer Mittwochs ab 19:00 Uhr.
